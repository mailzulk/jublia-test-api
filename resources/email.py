import os
import datetime
from flask import request
from flask_restful import Resource
from model import db, Email, EmailSchema
from pytz import timezone
from flask_mail import Mail, Message
from celery import Celery
from task import send_email

emails_schema = EmailSchema(many=True)
email_schema = EmailSchema()

# This is the script where we defined what operation would we like to execute with our API

class EmailsAPI(Resource):

    # This is the GET method to retrieve all email objects
    def get(self):
        emails = Email.query.all()
        emails = emails_schema.dump(emails).data
        return {'status': 'success', 'data': emails}, 200

    # This is the POST method to save email objects and queue it into Celery
    def post(self):
        if 'recipient' in request.form and 'subject' in request.form and 'content' in request.form and 'datetime' in request.form:
            email = Email(
            email_recipients = request.form['recipient'],
            email_subject = request.form['subject'],
            email_content = request.form['content'],
            timestamp = request.form['datetime']
            )
            db.session.add(email)
            db.session.commit()

            # This is the command which we use to redirect the saved email to Celery queue to send email in its predetermined timestamp
            send_email.apply_async((email.event_id,), eta=email.timestamp)

            result = email_schema.dump(email).data
            return { "status": 'success', 'data': result }, 201
        else:
            return { "status": 'failed', 'message': 'form not complete' }, 400
    