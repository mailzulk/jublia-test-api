from flask import Blueprint
from flask_restful import Api
from resources.email import EmailsAPI

# Route initialization for API

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(EmailsAPI, '/save_emails')
