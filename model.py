from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

ma = Marshmallow()
db = SQLAlchemy()

# This is the model definition for our object which we want to work with

class Email(db.Model):
    event_id = db.Column(db.Integer, primary_key=True)
    email_recipients = db.Column(db.String(120))
    email_subject = db.Column(db.String(120))
    email_content = db.Column(db.Text)
    timestamp = db.Column(db.DateTime(timezone=True))

    def __init__(self, email_recipients, email_subject, email_content, timestamp):
        self.email_recipients = email_recipients
        self.email_subject = email_subject
        self.email_content = email_content
        self.timestamp = timestamp

class EmailSchema(ma.Schema):
    event_id = fields.Integer()
    email_recipients = fields.String(required=True)
    email_subject = fields.String(required=True)
    email_content = fields.String(required=True)
    timestamp = fields.DateTime()