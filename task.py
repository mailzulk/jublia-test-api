from celery import Celery
import os
from flask_mail import Mail, Message
from model import db, Email, EmailSchema

# This is the initialization script for Celery

def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery

from flask import Flask

flask_app = Flask(__name__)
flask_app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
flask_app.config.from_object('config')
db.init_app(flask_app)

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    # email username
    "MAIL_USERNAME": <your-email-username>,
    # email password
    "MAIL_PASSWORD": <your-email-password>
}
flask_app.config.update(mail_settings)

mail = Mail()
mail.init_app(flask_app)
celery = make_celery(flask_app)

# This is the script which we use to queue task with Celery to send e-mail

@celery.task
def send_email(event_id):
    with flask_app.app_context():
        # email username
        email_sender = <your-email-username>
        mail = Mail()
        email = Email.query.filter_by(event_id=event_id).first()
        msg = Message(subject=email.email_subject,
                        sender=email_sender,
                        recipients=[email.email_recipients],
                        body=email.email_content)
        mail.send(msg)
        return {'status': 'success', 'message': 'email sent!'}, 200